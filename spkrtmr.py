#! python
'''
Created on Mar 5, 2018

@author: kooliah
'''
import os, getopt, sys

try:
    from Tkinter import *
except Exception:
    from tkinter import *
import time

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

def usage():
    print( read('README.md'))


class config(object):
    def __init__(self, _seconds,_alarm,_fc,_bc,_size):        
        """ Custom settings. """
        self.FG_col=_fc # Foreground                                                                
        self.BG_col=_bc # Background
        self.Ttimer=int(_seconds) # Countdown seconds
        self.Fsize=_size # Timer font size
        self.Msize=20  # Blocks font size
        self.Aalarm=int(_alarm) # Flashing seconds                                                               

class SpeakerTimer(Frame):  
    """ Implements a Timer frame widget. """
    def __init__(self, _cnf, parent=None ,**kw):        
        Frame.__init__(self, parent, kw)
        parent.title('AlkTimer')
        self._tmr = _cnf.Ttimer
        self._remaining = self._tmr
        self._elapsedtime = 0.0
        self._start = 0.0        
        self._alrm=_cnf.Aalarm
        self._blocks=0
        self._running = 0
        self.timestr = StringVar()               
        self.blckstr = StringVar()               
        self._blck = Label(self, textvariable=self.blckstr, font = "Helvetica %s bold" % (_cnf.Msize,) ,fg=_cnf.FG_col,bg=_cnf.BG_col)
        self._lbl = Label(self, textvariable=self.timestr, font = "Helvetica %s bold" % (_cnf.Fsize,),fg=_cnf.FG_col,bg=_cnf.BG_col)
        self.cnf=_cnf
        self.makeWidgets()      
                                     
    def makeWidgets(self):                         
        """ Make the time and blocks label. """
        self._setTime(self._remaining)
        self._setmin(self._blocks)
        self._lbl.pack(fill=X, expand=NO, pady=2, padx=2)                      
        self._blck.pack( expand=NO, pady=2, padx=2)                      

    def flash(self):
        """ Flash label. """
        bg = self._lbl.cget("background")
        fg = self._lbl.cget("foreground")
        self._lbl.configure(background=fg, foreground=bg)

    def _update(self): 
        """ Update the label with elapsed time. """
        self._setElapsed(time.time()-self._start)  
        self._setTime(self._remaining)
        self._timer = self.after(100, self._update)
        if self._remaining<self._alrm:
            self.flash()
            if self._remaining<=0.0:
                self.StartStop()
                self.Addmin(1)

    def _setmin(self, minuto):
        """ Set blocks number """
        self.blckstr.set('%02d' % (minuto))
    
    def _setTime(self, elap):
        """ Set the time string to Minutes:Seconds:Hundreths """
        minutes = int(elap/60)
        seconds = int(elap - minutes*60.0)
        hseconds = int((elap - minutes*60.0 - seconds)*10)                
        self.timestr.set('%02d:%02d:%01d' % (minutes, seconds, hseconds))
        
    def Addblck(self, qta):
        """ Add block """
        self._blocks+=qta
        self._setmin(self._blocks)                                                     
        
    def StartStop(self, event=0):                                                     
        """ Start the timer, ignore if running. """
        if self._remaining<=0.0:
            self.Reset()
        
        if not self._running: 
            self._start = time.time() - self._elapsedtime
            self._update()
            self._running = 1
        else:        
            self.after_cancel(self._timer)            
            self._setElapsed(time.time() - self._start)    
            self._setTime(self._remaining)
            self._running = 0
    
    def _setElapsed(self,elap):                                  
        self._elapsedtime = elap    
        self._remaining = self._tmr-self._elapsedtime
          
    def Reset(self, event=0):                                  
        """ Reset the Timer. """
        if not self._running:
            self._start = time.time()         
            self._elapsedtime = 0.0    
            self._remaining = self._tmr-self._elapsedtime  
            self._setTime(self._remaining)
            self._lbl.configure(background=self.cnf.BG_col, foreground=self.cnf.FG_col)

def main(argv=['-h']):
    _alarm=10
    _seconds=0
    _fc='white smoke'
    _bc='sea green'
    _size=200
    try:                                
        opts, args = getopt.getopt(argv, "a:s:f:b:z:h", ["alarm=",'seconds=','forecolor=','backcolor=','size=']) 
    except getopt.GetoptError:           
        usage()                          
        sys.exit() 
    if not opts:
        usage()
        sys.exit() 
    for opt, arg in opts: 
        if opt in ('-a', '--alarm'):                
            _alarm= arg
        elif opt in ('-f', '--forecolor'):
            _fc= arg
        elif opt in ('-h'):
            usage()
            sys.exit() 
        elif opt in ('-b','--backcolor'):
            _bc= arg
        elif opt in ('-s', '--seconds'):
            _seconds=arg
        elif opt in ('-z', '--size'):
            _size=arg
        else:
            print( "Not a valid option") 
            usage()
            sys.exit() 
    if _seconds:
        root = Tk()
        _cnf=config(_seconds,_alarm,_fc,_bc, _size  )
        sw = SpeakerTimer(_cnf, root )
        sw.pack(side=TOP)
        
        Button(root, text='Start/Stop', command=sw.StartStop).pack(side=LEFT)
        Button(root, text='Reset', command=sw.Reset).pack(side=LEFT)
        Button(root, text='Quit', command=root.quit).pack(side=LEFT)
        Button(root, text='+', command=lambda: sw.Addblck(1)).pack(side=LEFT)
        Button(root, text='-', command=lambda: sw.Addblck(-1)).pack(side=LEFT)
        sw.focus_set()
        sw.bind('<space>', sw.StartStop)
        sw.bind("<Alt_L>", sw.Reset)
        sw.pack()
        
        root.mainloop()
    else:
        usage()

if __name__ == '__main__':

#     _par=['-s', 20,
#       '-a', 5,
#        ]
#     main(_par)
    
    main(sys.argv[1:])
