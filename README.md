# README #

### Description ###

Countdown timer and blocks numbering for radio speakers or everyone needs a timer with a customizable interface.  
Blocks number is automatically updated when countdown reachs 0, anyway it can be modified by buttons.  
 
* Version 1
* Support Python 2/3  

![Screenshot](http://www.alkatron.net/static/imgs/shared/timer.png)

### How do I get set up? ###

	git clone https://alkatron@bitbucket.org/alkatron/speaker_timer.git  
	
### Dependencies ###
python-tk or python3-tk respectively.  

### Configuration ###
* To choose color name from the color table  

	python3 tkcolor.py  
	
	
### Keyboard commands ###
* <space> Start/Stop
* <left alt> Reset

### Known problems ###

	python3 spkrtmr.py [options]
	...
	...
	import _tkinter # If this fails your Python may not be configured for Tk
	ModuleNotFoundError: No module named '_tkinter'

Run it using /usr/bin/python3 instead of simple python3  

	/usr/bin/python3 spkrtmr.py [options]
	
### Console usage ###
	python spkrtmr.py [options]  

Options:  

-s, --seconds     :Countdown seconds (required)  
-a, --alarm      :Alarm seconds (optional default 10)  
-f, --forecolor    :Foreground color (optional default 'white smoke')  
-b, --backcolor   :Background color (optional default 'sea green')  
-z, --size   :Font size (optional default 200)  

Example:  

	python spkrtmr.py -s 15 -a 5

[Contacts](http://alkatron.net/email/)
